$(document).ready(function() {

  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    });
  }

    $("input").change(function() {
        if (this.checked) {
            $('link[href="/static/css/stylelight.css"]').attr('href', '/static/css/styledark.css');
            document.getElementById('mode').innerHTML = '<strong>Dark Mode</strong>';
        } else {
            $('link[href="/static/css/styledark.css"]').attr('href', '/static/css/stylelight.css');
            document.getElementById('mode').innerHTML = '<strong>Light Mode</strong>';
        }
    });


});
