from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from django.test import LiveServerTestCase
from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options

# # Create your tests here.

class Story7UnitTest(TestCase):

    def test_url_exist(self):
        response = self.client.get('/story7')
        self.assertEqual(response.status_code,200)

    def test_template(self):
        response = self.client.get('/story7')
        self.assertTemplateUsed(response,'story7.html')

    def test_greetings(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('HOWDY, PARTNER!',html_response)

class Story7FuncTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000/story7')

    def tearDown(self):
        self.selenium.quit()
