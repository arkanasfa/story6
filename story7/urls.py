from django.urls import path
from .views import index


appname = 'story7'

urlpatterns = [
   path('', index, name = 'accordion'),
]
