from django import forms
from .models import Update

class KontenForm(forms.ModelForm):
    class Meta:
        model   = Update
        fields  = [
            'status',
        ]
        widgets ={
            'status' : forms.TextInput(
                attrs = {
                    'class'         : 'form-control',
                    'placeholder'   : 'apa kabarmu?',
                }
            )
        }
