from django.db import models

# Create your models here.

class Update(models.Model):
    time = models.DateTimeField(auto_now_add = True)
    status = models.CharField(max_length      = 300)
