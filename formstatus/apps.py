from django.apps import AppConfig


class FormstatusConfig(AppConfig):
    name = 'formstatus'
